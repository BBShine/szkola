<!DOCTYPE html>
<html>
<head>
  <link rel = "stylesheet"  href = "<?php echo base_url(); ?>css/style.css">
  <link rel = "stylesheet"  href = "<?php echo base_url(); ?>css/bootstrap.css">
  <script type = 'text/javascript' src = "<?php echo base_url(); ?>js/jquery.js"></script>
  <script type = 'text/javascript' src = "<?php echo base_url(); ?>js/bootstrap.js"></script>
  <script type = 'text/javascript' src = "<?php echo base_url(); ?>js/site.js"></script>
  <meta charset="utf-8"/>
  <title>Szkoła</title>
</head>
<body>
  <nav class="navbar navbar-expand-sm sticky-top navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Szkoła</a>

    <ul class="nav navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="#">Home </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
        </div>
      </li>
    </ul>

  </nav>
